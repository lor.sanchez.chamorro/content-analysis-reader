# encoding: utf-8

'''
text_processor -- Classifies the words in a text by type (noun, adjective, verb...).
'''

from argparse import ArgumentParser
import json
import sys

import nltk  # pip install nltk

DEFAULT_LANG = 'english'
FILE_ENCODING = 'utf-8'
COMPACT_INDENT = 4

# https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
TAGS_BY_TAG_NAME = {
    'nouns': ['NN', 'NNS'],
    'propperNouns': ['NNP', 'NNPS'],
    'adjectives': ['JJ', 'JJR', 'JJS'],
    'adverbs': ['RB', 'RBR', 'RBS'],
    'verbs': ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'],
    'numbers': ['CD']
}

TAG_NAMES_BY_TAG = {}
for tagName in TAGS_BY_TAG_NAME:
    for tag in TAGS_BY_TAG_NAME[tagName]:
        TAG_NAMES_BY_TAG[tag] = tagName


def processText(text, lang=DEFAULT_LANG):
    
    tokens = nltk.word_tokenize(text, lang)
    tagged_tokens = nltk.pos_tag(tokens)
    
    # Tokens by tagName name.
    tokensByTagName = {}
    
    for tagName in TAGS_BY_TAG_NAME:
        tokensByTagName[tagName] = []
    
    tokensByTagName['others'] = []
    
    for token, tagName in tagged_tokens:
        if tagName in TAG_NAMES_BY_TAG:
            tokensByTagName[TAG_NAMES_BY_TAG[tagName]].append(token)
        else:
            tokensByTagName['others'].append(token)
    
    # Tokens count by tagName name.
    tokensCountByTagName = {}
    
    for tagName in tokensByTagName:
        tokensCountByTagName[tagName] = len(tokensByTagName[tagName])
        
    tokensCountByTagName['total'] = len(tokens)
    
    return tokensByTagName, tokensCountByTagName


def main(argv=None):

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    try:

        # Setup argument parser.
        parser = ArgumentParser(description='Classifies the words in a text by type (noun, adjective, verb...).')
        parser.add_argument('-f', '--file', required=True, dest='file', help='the text file to process')
        parser.add_argument('-l', '--lang', required=False, default=DEFAULT_LANG, dest='lang', help='the text language (default: {})'.format(DEFAULT_LANG))
        parser.add_argument('-oc', '--only-count', action='store_true', dest='onlyCount', help='show only the words count by tag')
        parser.add_argument('-ow', '--only-words', action='store_true', dest='onlyWords', help='show only the words by tag')
        parser.add_argument('-c', '--compact', action='store_true', dest='compact', help='compact the output')

        # Process the arguments.
        args = parser.parse_args()
        file = args.file
        lang = args.lang
        onlyCount = args.onlyCount
        onlyWords = args.onlyWords
        compact = args.compact
        
        # Prepare the arguments.
        file = file.strip()
        with open(file, 'r', encoding=FILE_ENCODING) as f:
            text = f.read()
            
        lang = lang.strip()
            
        # Process the text.
        wordsByTagName, wordsCountByTagName = processText(text, lang)

        # Print the output.
        if onlyCount:
            output = wordsCountByTagName
        elif onlyWords:
            output = wordsByTagName
        else:
            output = {
                'count': wordsCountByTagName,
                'words': wordsByTagName
            }
        
        print(json.dumps(output, indent=None if compact else COMPACT_INDENT))

        return 0

    except KeyboardInterrupt:
        return 0

    except Exception as e:
        raise(e)


if __name__ == '__main__':
    sys.exit(main())
